﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace texte_de.business
{
    class Bbusiness
    {
        public void insert (model.Bmodel model)
        {
            if (model.nome == string.Empty)
            {
                throw new  ArgumentException ("informe seu nome");
                
                
            }
            if (model.cabelo == "Não" && model.barba == "Não")
            {
                throw new ArgumentException ("escolha um serviço");
            }

            database.Bdatabase data = new database.Bdatabase();
            data.insert(model);
        }
        public List<model.Bmodel> Filtrar (string cliente, DateTime dia)
        {
            database.Bdatabase data = new database.Bdatabase();

            List<model.Bmodel> lista = data.Filtrar(cliente, dia);
            return lista;
        }

        public model.Bmodel buscarID ( int Id )
        {
            database.Bdatabase data = new database.Bdatabase();
            model.Bmodel model = data.buscar(Id);

            return model;

        }
        public void deletar (int Id)
        {
            database.Bdatabase data = new database.Bdatabase();
            data.deletar(Id);
        }

    }
}
