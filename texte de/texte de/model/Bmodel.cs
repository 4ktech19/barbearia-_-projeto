﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace texte_de.model
{
    class Bmodel
    {
        public int id { get; set; }
        public string nome { get; set; }
        public string cabelo { get; set; }
        public string barba { get; set; }
        public DateTime dia { get; set; }
        public TimeSpan hora { get; set; }


    }
}
