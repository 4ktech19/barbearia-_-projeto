﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace texte_de.telas
{
    public partial class frmMenuBarbeiro : Form
    {
        public frmMenuBarbeiro()
        {
            InitializeComponent();
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            string cliente = txtCliente.Text;
            DateTime dia = dtDia1.Value.Date;


            business.Bbusiness busi = new business.Bbusiness();
            List<model.Bmodel> lista = busi.Filtrar(cliente, dia);

            grid.DataSource = lista;
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            frmClienteEscolha tela = new frmClienteEscolha();
            tela.Show();
            this.Hide();
        }
    }
}
        

       
