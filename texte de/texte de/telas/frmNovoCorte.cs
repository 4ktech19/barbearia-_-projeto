﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace texte_de.telas
{
    public partial class frmNovoCorte : Form
    {
        public frmNovoCorte()
        {
            InitializeComponent();
        }

        private void BunifuDropdown1_onItemSelected(object sender, EventArgs e)
        {
            this.click();
        }
       
        public void click()
        {
            model.Bmodel model = new model.Bmodel();
            model.nome = txtnome.Text;
            model.cabelo = cbocabelo.Text;
            model.barba = cbobarba.Text;
            model.dia = dtdia.Value.Date;
            model.hora = TimeSpan.Parse(dthora.Text);

            if (model.nome == string.Empty)
            {
                MessageBox.Show("coloque seu nome");
                return;
            }

            if (model.cabelo == "Não" && model.barba == "Não")
            {
                MessageBox.Show("escolha um serviço");
                return;
            }
            business.Bbusiness busi = new business.Bbusiness();
            busi.insert(model);

            MessageBox.Show("Obrigado volte sempre", "Esperamos por você", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void PictureBox3_Click(object sender, EventArgs e)
        {
            this.click();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            frmloding tela= new frmloding();
            tela.Show();
            this.Hide();
        }
    }
}








