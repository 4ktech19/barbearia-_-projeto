﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace texte_de.telas
{
    public partial class frmCadastrar : Form
    {
        public frmCadastrar()
        {
            InitializeComponent();
        }





        private void BunifuFlatButton1_Click(object sender, EventArgs e)
        {
            string Usuario = txtuser.Text;
            string Senha1 = txtsenha.Text;
            string Senha2 = txtsenha2.Text;
            if (Senha1 == Senha2)
            {
                model.ModeloLogin LG = new model.ModeloLogin();
                LG.Usuario = Usuario;
                LG.Senha = Senha1;
                business.LoginBunisses LG1 = new business.LoginBunisses();
                LG1.inserir(LG);
                MessageBox.Show("Inserido com sucesso!", "INSERIDO", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Senhas não compatíveis");
                return;
            }

           
            frmlogincliente tela = new frmlogincliente();
            tela.Show();
            this.Hide();

        }

        private void txtuser_Enter(object sender, EventArgs e)
        {
            if (txtuser.Text == "User")
            {
                txtuser.Text = null;
            }
            
        }

        private void txtsenha_Enter(object sender, EventArgs e)
        {
            if(txtsenha.Text == "Senha")
            txtsenha.Text = null;
        }

        private void txtsenha2_Enter(object sender, EventArgs e)
        {
            if (txtsenha2.Text == "Confirme a Senha")
            txtsenha2.Text = null;
        }

        private void txtuser_Leave(object sender, EventArgs e)
        {
            if (txtuser.Text == "User" || txtuser.Text == string.Empty)
            {
                txtuser.Text = "User";
            }
        }

        private void txtsenha_Leave(object sender, EventArgs e)
        {
            if (txtsenha.Text == string.Empty || txtsenha.Text == "Senha")
            {
                txtsenha.Text = "Senha";
            }


        }

        private void txtsenha2_Leave(object sender, EventArgs e)
        {
            if (txtsenha2.Text == "Confirme a Senha" || txtsenha2.Text == string.Empty)
            {
                txtsenha2.Text = "Confirme a Senha";
            }

        }

        private void txtuser_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            frmlogincliente Tela = new frmlogincliente();
            Tela.Show();
            Tela.Hide();
        }
    }
}