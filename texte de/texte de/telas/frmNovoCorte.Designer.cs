﻿namespace texte_de.telas
{
    partial class frmNovoCorte
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtnome = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.cbocabelo = new System.Windows.Forms.ComboBox();
            this.cbobarba = new System.Windows.Forms.ComboBox();
            this.dthora = new System.Windows.Forms.MaskedTextBox();
            this.dtdia = new Bunifu.Framework.UI.BunifuDatepicker();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // txtnome
            // 
            this.txtnome.BackColor = System.Drawing.Color.Khaki;
            this.txtnome.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtnome.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtnome.ForeColor = System.Drawing.Color.Black;
            this.txtnome.HintForeColor = System.Drawing.Color.Empty;
            this.txtnome.HintText = "";
            this.txtnome.isPassword = false;
            this.txtnome.LineFocusedColor = System.Drawing.Color.Blue;
            this.txtnome.LineIdleColor = System.Drawing.Color.Gray;
            this.txtnome.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.txtnome.LineThickness = 3;
            this.txtnome.Location = new System.Drawing.Point(413, 78);
            this.txtnome.Margin = new System.Windows.Forms.Padding(4);
            this.txtnome.Name = "txtnome";
            this.txtnome.Size = new System.Drawing.Size(224, 44);
            this.txtnome.TabIndex = 1;
            this.txtnome.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // cbocabelo
            // 
            this.cbocabelo.BackColor = System.Drawing.Color.Khaki;
            this.cbocabelo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbocabelo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.cbocabelo.FormattingEnabled = true;
            this.cbocabelo.Items.AddRange(new object[] {
            "Sim",
            "Não"});
            this.cbocabelo.Location = new System.Drawing.Point(413, 136);
            this.cbocabelo.Name = "cbocabelo";
            this.cbocabelo.Size = new System.Drawing.Size(224, 28);
            this.cbocabelo.TabIndex = 2;
            // 
            // cbobarba
            // 
            this.cbobarba.BackColor = System.Drawing.Color.Khaki;
            this.cbobarba.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbobarba.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.cbobarba.FormattingEnabled = true;
            this.cbobarba.Items.AddRange(new object[] {
            "Sim",
            "Não"});
            this.cbobarba.Location = new System.Drawing.Point(413, 176);
            this.cbobarba.Name = "cbobarba";
            this.cbobarba.Size = new System.Drawing.Size(224, 28);
            this.cbobarba.TabIndex = 2;
            // 
            // dthora
            // 
            this.dthora.BackColor = System.Drawing.Color.Khaki;
            this.dthora.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.dthora.Location = new System.Drawing.Point(413, 268);
            this.dthora.Mask = "00:00";
            this.dthora.Name = "dthora";
            this.dthora.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dthora.Size = new System.Drawing.Size(224, 23);
            this.dthora.TabIndex = 3;
            this.dthora.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.dthora.ValidatingType = typeof(System.DateTime);
            // 
            // dtdia
            // 
            this.dtdia.BackColor = System.Drawing.Color.Khaki;
            this.dtdia.BorderRadius = 0;
            this.dtdia.ForeColor = System.Drawing.Color.Black;
            this.dtdia.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.dtdia.FormatCustom = null;
            this.dtdia.Location = new System.Drawing.Point(413, 216);
            this.dtdia.Name = "dtdia";
            this.dtdia.Size = new System.Drawing.Size(224, 33);
            this.dtdia.TabIndex = 4;
            this.dtdia.Value = new System.DateTime(2019, 6, 13, 14, 17, 8, 341);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(306, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(201, 79);
            this.label1.TabIndex = 5;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pictureBox3.Image = global::texte_de.Properties.Resources.plus;
            this.pictureBox3.Location = new System.Drawing.Point(660, 78);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(80, 213);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 7;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.PictureBox3_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pictureBox2.Image = global::texte_de.Properties.Resources.Prancheta_1_cópia;
            this.pictureBox2.Location = new System.Drawing.Point(40, 111);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(260, 149);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 6;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = global::texte_de.Properties.Resources.tela_novo_corte;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(784, 467);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Font = new System.Drawing.Font("Neon 80s", 14.25F);
            this.label2.Location = new System.Drawing.Point(454, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(134, 28);
            this.label2.TabIndex = 8;
            this.label2.Text = "Bem vindo:\r\n\r\n";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Font = new System.Drawing.Font("Neon 80s", 14.25F);
            this.label3.Location = new System.Drawing.Point(339, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 20);
            this.label3.TabIndex = 9;
            this.label3.Text = "Nome:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Font = new System.Drawing.Font("Neon 80s", 14.25F);
            this.label4.Location = new System.Drawing.Point(339, 136);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 20);
            this.label4.TabIndex = 9;
            this.label4.Text = "Cabelo:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Font = new System.Drawing.Font("Neon 80s", 14.25F);
            this.label5.Location = new System.Drawing.Point(339, 176);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 20);
            this.label5.TabIndex = 9;
            this.label5.Text = "Barba:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label6.Font = new System.Drawing.Font("Neon 80s", 14.25F);
            this.label6.Location = new System.Drawing.Point(339, 216);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 20);
            this.label6.TabIndex = 9;
            this.label6.Text = "Dia:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label7.Font = new System.Drawing.Font("Neon 80s", 14.25F);
            this.label7.Location = new System.Drawing.Point(339, 268);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 20);
            this.label7.TabIndex = 9;
            this.label7.Text = "Hora:";
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pictureBox4.Image = global::texte_de.Properties.Resources.reply;
            this.pictureBox4.Location = new System.Drawing.Point(733, 12);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(39, 31);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 10;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label8.Location = new System.Drawing.Point(27, 53);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 223);
            this.label8.TabIndex = 11;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label9.Location = new System.Drawing.Point(27, 83);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(296, 39);
            this.label9.TabIndex = 11;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label10.Location = new System.Drawing.Point(267, 111);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(56, 149);
            this.label10.TabIndex = 11;
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label11.Location = new System.Drawing.Point(40, 248);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(283, 55);
            this.label11.TabIndex = 11;
            // 
            // frmNovoCorte
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 467);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.dthora);
            this.Controls.Add(this.dtdia);
            this.Controls.Add(this.cbobarba);
            this.Controls.Add(this.cbocabelo);
            this.Controls.Add(this.txtnome);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmNovoCorte";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmNovoCorte";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtnome;
        private System.Windows.Forms.ComboBox cbocabelo;
        private System.Windows.Forms.ComboBox cbobarba;
        private System.Windows.Forms.MaskedTextBox dthora;
        private Bunifu.Framework.UI.BunifuDatepicker dtdia;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
    }
}