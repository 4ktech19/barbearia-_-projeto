﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace texte_de.telas
{
    public partial class frmThanks : Form
    {
        public frmThanks()
        {
            InitializeComponent();
        }

        private void BunifuFlatButton1_Click(object sender, EventArgs e)
        {
            telas.frmNovoCorte tela = new frmNovoCorte();
            tela.Show();
            this.Hide();
        }
    }
}
