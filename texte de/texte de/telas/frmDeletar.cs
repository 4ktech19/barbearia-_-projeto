﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace texte_de.telas
{
    public partial class frmDeletar : Form
    {
        public frmDeletar()
        {
            InitializeComponent();
        }

        private void BtnBuscar_Click(object sender, EventArgs e)
        {
            try 
            {
                int Id = Convert.ToInt32(txtID.Text);
                business.Bbusiness busi = new business.Bbusiness();
                model.Bmodel model = busi.buscarID(Id);

                if (model != null)
                {
                    txtcliente.Text = model.nome;
                    DateTime dia = model.dia;
                    dtDia.Text = Convert.ToString(dia);

                }
                else
                {
                    txtcliente.Text = string.Empty;
                    DateTime dia = model.dia;
                    dtDia.Text = string.Empty;
                }
            }
            catch
            {
                MessageBox.Show("ID não encontrado", "Error", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
        }
        
        private void BunifuTileButton1_Click(object sender, EventArgs e)
        {
            try
            {
                int Id = Convert.ToInt32(txtID.Text);
                business.Bbusiness busi = new business.Bbusiness();
                busi.deletar(Id);
                MessageBox.Show("Excluido com sucesso!", "Deletado", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            catch
            {
                MessageBox.Show("ID não encontrado", "Digite novamente", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void PictureBox2_Click(object sender, EventArgs e)
        {
            frmClienteEscolha tela = new frmClienteEscolha();
            tela.Show();
            this.Hide();
        }
    }
}
