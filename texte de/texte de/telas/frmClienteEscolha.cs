﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace texte_de.telas
{
    public partial class frmClienteEscolha : Form
    {
        public frmClienteEscolha()
        {
            InitializeComponent();
        }

        private void BunifuTileButton1_Click(object sender, EventArgs e)
        {
            frmNovoCorte tela = new frmNovoCorte();
            tela.Show();
            this.Hide();
        }

        private void BunifuTileButton2_Click(object sender, EventArgs e)
        {
            frmMenuBarbeiro tela = new frmMenuBarbeiro();
            tela.Show();
            this.Hide();
        }

        private void BunifuTileButton3_Click(object sender, EventArgs e)
        {
            frmDeletar tela = new frmDeletar();
            tela.Show();
            this.Hide();
        }

        private void Voltar_Click(object sender, EventArgs e)
        {
            frmloding tela = new frmloding();
            tela.Show();
            this.Hide();
        }
    }
}
