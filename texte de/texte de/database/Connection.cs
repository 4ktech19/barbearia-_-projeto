﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace texte_de.database
{
    class Connection
    {
        public MySqlConnection Create()
        {
            string connectionString = "server=localhost; database=bar_data_tex2; uid=root; password=1234";

            MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open();

            return connection;
        }
    }
}
