﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace texte_de.database
{
    class Bdatabase
    {
        public void insert(model.Bmodel model)
        {
            string script = @" insert into tb_barber (nm_nome, nm_cabelo, dt_dia, ds_barba, hr_hora)
                                       values (@nm_nome, @nm_cabelo, @dt_dia, @ds_barba, @hr_hora)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", model.nome));
            parms.Add(new MySqlParameter("nm_cabelo", model.cabelo));
            parms.Add(new MySqlParameter("dt_dia", model.dia));
            parms.Add(new MySqlParameter("ds_barba", model.barba));
            parms.Add(new MySqlParameter("hr_hora", model.hora));

            DB db = new DB();
            db.ExecuteInsertScript(script, parms);

        }
        public List<model.Bmodel> Filtrar(string cliente, DateTime dia)
        {
            string script = "select * from tb_barber where nm_nome like @nm_nome and dt_dia = @dt_dia";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", "%" + cliente + "%"));
            parms.Add(new MySqlParameter("dt_dia", dia));

            DB db = new DB();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<model.Bmodel> lista = new List<model.Bmodel>();

            while (reader.Read())
            {
                model.Bmodel model = new model.Bmodel();
                model.id = Convert.ToInt32(reader["id_cliente"]);
                model.nome = Convert.ToString(reader["nm_nome"]);
                model.cabelo = Convert.ToString(reader["nm_cabelo"]);
                model.barba = Convert.ToString(reader["ds_barba"]);
                model.dia = Convert.ToDateTime(reader["dt_dia"]);
                model.hora = TimeSpan.Parse(reader["hr_hora"].ToString());
                

                lista.Add(model);
            }
            reader.Close();

            return lista;
        }

        public void deletar (int Id)
        {
            string script = @"delete from tb_barber where id_cliente = @id_cliente";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", Id));

            DB db = new DB();
            db.ExecuteInsertScript (script, parms);
        }

        public model.Bmodel buscar (int Id)
        {
            string script = "select* from tb_barber where id_cliente = @id_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", Id));

            DB db = new DB();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            model.Bmodel model = null;

            if (reader.Read())
            {
                model = new model.Bmodel();
                model.id = Convert.ToInt32(reader["id_cliente"]);
                model.nome = Convert.ToString(reader["nm_nome"]);
                model.cabelo = Convert.ToString(reader["nm_cabelo"]);
                model.barba = Convert.ToString(reader["ds_barba"]);
                model.dia = Convert.ToDateTime(reader["dt_dia"]);
                model.hora = TimeSpan.Parse(reader["hr_hora"].ToString());
            }
            reader.Close();

            return model;
        }
    }
}
