﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace texte_de.database
{
    class LoginDataBase
    {
        public void Inserir(model.ModeloLogin LG)
        {
            string script = @"insert into Login (user_nome, Senha)
                                         values (@user_nome, @Senha)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("user_nome", LG.Usuario));
            parms.Add(new MySqlParameter("Senha", LG.Senha));

            DB db = new DB();
            db.ExecuteInsertScript(script, parms);
        }
        public model.ModeloLogin Login(string Usuario, string Senha)
        {
            string script = "select * from Login where user_nome = @user_nome and Senha = @Senha";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("user_nome", Usuario));
            parms.Add(new MySqlParameter("Senha", Senha));

            DB db = new DB();
            db.ExecuteInsertScript(script, parms);

            DB DB = new DB();
            MySqlDataReader reader = DB.ExecuteSelectScript(script, parms);

            model.ModeloLogin model = null;

            if (reader.Read())
            {
                model = new model.ModeloLogin();
                model.Id = Convert.ToInt32(reader["id"]);
                model.Usuario = Convert.ToString(reader["user_nome"]);
                model.Senha = Convert.ToString(reader["Senha"]);
            }
            reader.Close();
            return model;
        }
    }
}
